app.config(function($routeProvider, $locationProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'app/views/annonces/index.html', 
			controller: 'AnnoncesController'
		})
		.otherwise({redirectTo : '/'})
});